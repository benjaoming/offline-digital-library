type: movie
slug: dinner-with-the-president
title: Dinner with the President
menu_order: 100
live: true
caption: Filmmakers Sabiha Sumar and Sachithanandam Sathananthan ask what democracy
  means in Pakistan – where its main promoter is the Chief of the Army who assumed
  power through military coup, and where society often still functions according to
  older tribal rules for political and social life.
description: "*   Producer: __Sachithenandam Sathananthan__\n*   Director: __Sabiha\
  \ Sumar and Sachithanandam Sathananthan__\n*   Editor: __Albert Elings, Eugenie\
  \ Jansen, Calle Overberg__\n*   Duration: __52:30__\n*   Country: __Pakistan__\n\
  \n### ABOUT THIS FILM\n\nIt’s pretty easy to hate General Pervez Musharraf, who\
  \ staged a military coup in Pakistan in 2001. Or is it? Since he took office he\
  \ has been described both as a military dictator and as a moderate and fairly progressive\
  \ leader. Indeed, President Musharraf’s government lacks the legitimacy that stems\
  \ from popular approval because he was not democratically elected. But, since he\
  \ took power, Pakistan has experienced moderate economic growth. Similarly, in other\
  \ parts of the world, non-elected leaders such as Deng Xiaoping in China have enabled\
  \ immense economic growth and stability through dictatorship. Even if they are not\
  \ democratically elected, these leaders might still be acting in the best interest\
  \ of a country and its citizens. By what do we measure a good leader? Are dictators\
  \ ever good?\n\nWhat are the implications for democracy in Pakistan when secular\
  \ political parties have succumbed to the Islamic agenda? What does it mean when\
  \ the army appears to be the only force able to contain the opponents of democracy,\
  \ the armed Islamists? President Musharraf agrees to explore this apparent contradiction\
  \ over dinner at his official residence, the Army House. As the discussion moves\
  \ in and out of the different worlds in Pakistan a complex tapestry emerges revealing\
  \ a society unique yet universal. The filmmaker talks to diverse individuals, from\
  \ labourers to intellectuals, from street vendors to religious right wing political\
  \ party members, and from journalists to industrialists. What is their idea of democracy\
  \ in Pakistan? What is their idea of President Musharraf’s vision of a modern Pakistan?\
  \ Dinner With the President questions the role a military leader can play in guiding\
  \ a state towards modern democracy.\n\n### KEY ISSUES\n\n*   Military rule\n*  \
  \ Dictatorship\n*   Tribal Leaders\n*   Urban-Rural Divide\n*   International Aid\n\
  \n### DISCUSSION QUESTIONS\n\n*   What are your thoughts about the responsibilities\
  \ of a country’s leaders?\n*   How is Musharraf portrayed by the various groups\
  \ in this film?\n*   Does an army General in uniform promote democracy? Explain\
  \ why or why not.\n*   Identify any gaps that seem to exist between rural and urban\
  \ groups in this film.  \n    Explain how and why they may have occurred.\n*   What\
  \ are some of the complexities about Pakistani society highlighted in this film?\n\
  *   What suggestion would you offer to improve this democracy?\n\n### DIRECTOR BIOGRAPHY\n\
  \nBorn in Karachi, SABIHA SUMAR studied Filmmaking and Political Science at Sarah\
  \ Lawrence College in New York and then read History and Political Thought at the\
  \ University of Cambridge. Who Will Cast the First Stone (1988) was her first film\
  \ (for Channel Four Television, UK). It won the Golden Gate Award at the San Francisco\
  \ Film Festival and was also screened at In Visible Colors, Vancouver. She has gone\
  \ on to direct many other films including Khamosh Pani (Silent Waters 2003), a feature\
  \ film which received the Silver Grand Balloon for Second Best Film at the 3 Continents\
  \ Festival, France, and the the Golden Leopard for Best Film at the Locarno International\
  \ Film Festival. In 2003 she also documented her personal journey through the Islamisation\
  \ process in Pakistan in Hawa Kay Naam (For a Place Under the Heavens). Both these\
  \ films were produced by co-director SACHITHANANDAM SATHANANTHAN, who holds a PhD\
  \ from the University of Cambridge, UK, and has previously produced Where Peacocks\
  \ Dance (1992), Of Mothers, Mice And Saints (1994), and Suicide Warriors (1996).\n\
  \n### FILM CONTEXT\n\nIn 1999 elected president Nawaz Sharif was ousted by a military\
  \ coup led by Army General Musharraf. Since then Musharraf has led the country.\
  \ In 2001 he was inaugurated as president of Pakistan. Musharraf argues that he\
  \ is facilitating democracy, but to what extent is democracy possible in a country\
  \ where only about half the country is literate and large factions of society are\
  \ living in abject poverty, ruled in accordance to a mix of tribal and religious\
  \ laws?\n\n### POLITICAL HISTORY\n\nPakistan was created in 1947, after an independence\
  \ struggle led by Muhammed Ali Jinnah, and is one of the largest Muslim countries\
  \ in the world. For the past 60 years Pakistani politics have been defined partially\
  \ by religion, and have always had close ties to the military. During this time\
  \ Pakistan has experienced several wars, has seen the rise of an extensive refugee\
  \ community from Afghanistan, and the government has seen four military coups. The\
  \ latest coup was in 1999 when, then Army General Musharraf returned from a trip\
  \ to Sri Lanka and was refused entry into the country and informed that he had been\
  \ removed from his post. Musharraf led the coup which effectively ousted then prime\
  \ minister Nawaz Sharif, then took office and informally instated martial law, with\
  \ consent from the courts and large sections of the parliament. He has promised\
  \ to bring democracy to Pakistan.\n\n### THE POLITICAL SCENE\n\nThe Pakistani government\
  \ is a federal republic. It has a semi-presidential system where the president is\
  \ the head of state and the prime minister, elected by the people, is the head of\
  \ government. The legislative power is vested in the parliament and the executive\
  \ power rests with the president and his cabinet. In 1999 Musharraf took office.\
  \ He argued that he was protecting the country from political turmoil and protecting\
  \ democracy. Musharraf has held national elections for local government in 2001\
  \ and won a vote of confidence from the Electoral College of Pakistan in 2004. The\
  \ 2001 elections were heavily boycotted and supposedly rigged. Musharraf argues\
  \ that he is facilitating development, and protecting the country from the rise\
  \ of religious extremism. Nonetheless, in the past years the religious party Muttahida\
  \ Majlis-e-Amal, also known as the MMA, has gained power winning an absolute majority\
  \ in the 2001 regional elections to gain control of the North-Western Province.\n\
  \n### WORLD RELEVANCE\n\nIt can be argued that Musharraf from the very beginning\
  \ faced an impossible situation. Stuck between the military, the religious extremists\
  \ and the U.S. war on terrorism in Iraq and Afghanistan, governing Pakistan was\
  \ never going to be an easy task. Musharraf was originally backed by the U.S. and\
  \ has supported the war on terrorism. However, as the war on terrorism has become\
  \ unpopular, religious parties in Pakistan have gained momentum. Meanwhile, Pakistan\
  \ needs the aid from the U.S. to sustain its military control and for socio-economic\
  \ development. So as with many countries in the world it has come down to a balancing\
  \ act between doing what is necessary to receive aid and please the public. Frequently\
  \ these goals have turned out to be contradictory.\n\n### LOOKING AHEAD\n\nSince\
  \ the movie was made, Musharraf has stepped down as head of Pakistan’s military\
  \ (in November 2007) under heavy domestic and international pressure.\n\nThe most\
  \ influential political parties in Pakistan are the Pakistan Peoples Party, the\
  \ MMA and the Pakistan Muslim League, which is divided into two factions, the PML-Q\
  \ and the PML-N. The PML-Q was formed by Musharraf in 2001 and is composed mainly\
  \ of former members of the older PML-N party. The Pakistan’s Peoples Party, or PPP,\
  \ was led by Benazir Bhutto, the daughter of the original founder of the party,\
  \ Zulfikar Ali Bhutto. Benazir Bhutto left politics and the country under a cloud\
  \ of corruption and, like Sharif, only recently returned to Pakistan and the stormy\
  \ political scene. Benazir was assassinated at a political rally prior to the elections,\
  \ but her party nonetheless was instrumental in ousting Musharraf’s party from power.\
  \ Though humbled, Musharraf remains the country’s leader, and Pakistan’s political\
  \ – and democratic – future remains uncertain."
license: null
resource_link: http://static.fair/movies/why_democracy/dinner_with_the_president.pls
year: null
author: Sabiha Sumar and Sachithanandam Sathananthan
country: ''
duration: '52:30'
icon_predefined: null
retrieved: null
disk_path_check: null
thumbnail_file: dinner-with-the-president.thumb.jpg
