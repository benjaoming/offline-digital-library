type: movie
slug: give-us-the-money
title: Give us the Money
menu_order: 100
live: true
caption: From Live Aid to Make Poverty History, celebrities have become activists
  against poverty. Bob Geldof and Bono have been the most prominent voices advocating
  on behalf of the poor. Geldof, Bono and Bill Gates speak candidly about how to lobby
  effectively and how to play to politicians’ weaknesses for glitz and popularity.
description: "## Outline\n\nThe success of Live Aid showed that music and entertainment\
  \ couldfocus popular attention on the needs of the world’s poor and demandchange\
  \ that the politicians could not ignore.\n\nMost prominent among this generation\
  \ of celebrity activists arethe Irish musicians Bob Geldof and Bono. From small\
  \ beginnings theycreated a sizable lobbying movement.\n\nIn&nbsp;Give Us The Money,\
  \ Geldof, Bono and fellow activist BillGates speak candidly about the lobbying,\
  \ strategizing and backroomdeals involved in their near 30 years of activism and\
  \ the valuablepolitical currency of celebrity.\n\nAs their political influence increased,\
  \ so have the questions,with critics wanting to know why celebrities have become\
  \ theself-appointed spokespeople for Africa’s poor.\n\nBut take a minute to&nbsp;\
  \ consider what it achieved. The Band Aidsingle&nbsp;Feed the World&nbsp;was the\
  \ fastest selling single everand raised £8 million. Live Aid’s concert for Africa\
  \ raised triplethe £10m expected.\n\nWhen the G8 Summit met in Cologne in 1999,\
  \ the Drop the Debtcampaign had collected 17 million signatures. The leaders of\
  \ the G8pledged to cancel $100 billion of debt for 42 heavily indebtedcountries.\n\
  \nFor Make Poverty History, millions of people wore white bands. Inthe UK alone,\
  \ 444,000 people emailed the Prime Minister about povertyand 225,000 took to the\
  \ streets of Edinburgh for the Make PovertyHistory march and rally.\n\n### In figures…\n\
  \nIt’s difficult to measure the effectiveness of aid. However, inthe last 10 years:\n\
  \n*   \n    \n    HIV treatment has reached&nbsp;6\tmillion&nbsp;people. \t\n  \
  \  \n    \n*   \n    \n    Malaria deaths\thave&nbsp;halved&nbsp;in&nbsp;8&nbsp;countries.\
  \ \t\n    \n    \n*   \n    \n    <a></a>1,700&nbsp;fewer\tchildren die&nbsp;per\
  \ day. \t\n    \n    \n*   \n    \n    At the G8 summit in 2005 a deal was made\
  \ to wipe out&nbsp;$40bn\t(£23bn)&nbsp;of debt owed by18 of the world’s poorest\
  \ countries. \t\n    \n    \n\n&nbsp;\n\nWatch an interview with the film’s Director&nbsp;[BosseLindquist](http://www.whypoverty.net/bosse-lindquist-give-us-money/)&nbsp;or\
  \ read an article on&nbsp;[whatprompted him](http://www.whypoverty.net/bosse-lindquist-give-us-money/)&nbsp;to\
  \ make the film.\n\n## Key Issues\n\n*   \n    \n    [Aid\tand Charity](http://www.whypoverty.net/fg-aid-charity/)\n\
  \    \n    \n*   \n    \n    [Food\tsecurity](http://www.whypoverty.net/fg-food-security/)\n\
  \    \n    \n*   \n    \n    [Inequality](http://www.whypoverty.net/fg-inequality/)\n\
  \    \n    \n*   \n    \n    Governance \t\n    \n    \n\n## What Can I Do?\n\n\
  Please share this film and encourage people to start askingquestions. It’s an easy\
  \ but effective way to keep the debate aboutpoverty going.\n\n&nbsp;\n\n## Show\
  \ It\n\nTo show this film to an audience,&nbsp;or screen it at afestival\\*, you\
  \ can:\n\n*   \n    \n    [Download\tGive Us The Money](https://vimeo.com/52221541)&nbsp;from\
  \ Vimeo \t\n    \n    \n*   \n    \n    Request a [DVD\tbox](http://www.whypoverty.net/boxset/)\n\
  \    \n    \n*   \n    \n    Use our [photos](https://www.dropbox.com/sh/qf5oytk6sth1nvs/AABhwtC-IgILZPvnxEyoS18Ea/GIVE%20US%20THE%20MONEY?dl=0?lst)\t\
  (film production still) in your material \t\n    \n    \n*   \n    \n    Feature\
  \ our [logo](https://www.dropbox.com/sh/1th9ar3oubgs3ew/AAC-0_MeTo_XBn8EeY8X6Ed0a?dl=0)\n\
  \    \n    \n*   \n    \n    [Contact\tus](http://www.whypoverty.net/contact/)&nbsp;if\
  \ you need any further help or information \t\n    \n    \n\n(\\*Please credit us\
  \ and read our [Terms&amp; Conditions](http://www.whypoverty.net/terms-conditions/)\
  \ before using our content)\n\n&nbsp;\n\n## Discuss It\n\n*   \n    \n    How did\
  \ Bono and Bob Geldof use\ttheir status for their campaigns? \t\n    \n    \n* \
  \  \n    \n    Do celebrities have political and\tsocial responsibilities? Give\
  \ examples. \t\n    \n    \n*   \n    \n    What have the different campaigns\t\
  achieved? \t\n    \n    \n*   \n    \n    What can be learned from the\tstrategies\
  \ applied in the campaigns? \t\n    \n    \n*   \n    \n    Some African grass roots\t\
  organizations felt undermined. Why? Whose voices should lead such\tcampaigns? \t\
  \n    \n    \n*   \n    \n    The campaign ‘Make Poverty\tHistory’ worked closely\
  \ with an Ethiopian dictator. Does the end\tjustify the means? \t\n    \n    \n\
  *   \n    \n    Gebru Asrat says, ‘If\tauthoritarians still remain in power, poverty\
  \ cannot be made\thistory.’ Do you agree? Explain. \t\n    \n    \n*   \n    \n\
  \    Bono claims that ‘The African people do not want aid as an\tongoing basis.\
  \ They need it now.’ What does this say about relief\tand foreign aid? \t\n    \n\
  \    \n\n&nbsp;\n\nTo discuss this and our other films with an audience, we’ve puttogether\
  \ a [Facilitator’sGuide&nbsp;PDF](https://www.dropbox.com/home/Why%20Poverty/Why%20Poverty%20Website/website%20content/Facilitators)\
  \ with ideas and suggestions on how to use our filmsas a teaching tool or starting\
  \ point for discussions.\n\n&nbsp;\n\n## Share It\n\nWe invite you to share your\
  \ experiences. Please join thediscussion and help us encourage people to start asking\
  \ questions on:\n\n[Facebook atWhyPoverty?](https://www.facebook.com/WhyPoverty)\n\
  \n[Twitter @askwhypoverty](https://twitter.com/askwhypoverty)\n\n## Join Others\n\
  \nYou can also join local organisations in your area orinternational groups working\
  \ on this issue:\n\n*   \n    \n    [Oxfam](http://www.oxfam.org/):\tthe global\
  \ development charity \t\n    \n    \n*   \n    \n    [Save\tthe Children](https://www.savethechildren.net/):\
  \ an international children’s charity which\tsupports both emergency and long-term\
  \ relief and development\tprojects. \t\n    \n    \n*   \n    \n    [One](http://www.one.org/international/):&nbsp;a\t\
  grass roots advocacy and campaigning organisation that fights\textreme poverty and\
  \ preventable disease, particularly in Africa. \t\n    \n    \n*   \n    \n    [The\t\
  Global Poverty Project](http://www.globalpovertyproject.com/):&nbsp;a research and\
  \ advocacy\torganisation working to stop&nbsp;illicit financial flows from\tdeveloping\
  \ countries. \t\n    \n    \n*   \n    \n    [/The Rules](http://www.therules.org/):\
  \ working to\tchange the rules that create poverty and inequality. \t\n    \n  \
  \  \n\n&nbsp;\n\nDirector: Bosse Lindquist\n\nProducer: David Herdies\n\nProduced\
  \ by: Momento Film\n\nCountries: Ethiopia, USA, UK\n\nYear: 2012\n\nLength: 58 minutes\n\
  \n  "
license: null
resource_link: http://static.fair/movies/why_poverty/give_us_the_money/movie.ogv
year: null
author: Bosse Lindquist
country: ''
duration: '58:08'
icon_predefined: null
retrieved: null
disk_path_check: null
