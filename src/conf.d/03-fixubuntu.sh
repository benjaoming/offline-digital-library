#!/bin/bash

# Some ideas from http://fixubuntu.com

# Various new fixes that should be used on an offline server

# Remove snapd and zeitgeist and deja-dup and amazon web launcher
sudo apt purge -y snapd zeitgeist deja-dup ubuntu-web-launchers
# Install packages that were snaps before
sudo apt install -y gnome-calculator gnome-system-monitor gnome-system-log



GS="/usr/bin/gsettings"
CCUL="com.canonical.Unity.lenses"

# Figure out the version of Ubuntu that you're running
V=`/usr/bin/lsb_release -rs`

# Check Canonical schema is present. Take first match, ignoring case.
SCHEMA="`$GS list-schemas | grep -i $CCUL | head -1`"
if [ -z "$SCHEMA" ]
  then
  printf "Error: could not find Canonical schema %s.\n" "$CCUL" 1>&2
  exit 1
else
  CCUL="$SCHEMA"
fi

# Turn off "Remote Search", so search terms in Dash don't get sent to the internet
$GS set $CCUL remote-content-search none

# Block connections to Ubuntu's ad server, just in case
if ! grep -q "127.0.0.1 productsearch.ubuntu.com" /etc/hosts; then
  echo -e "\n127.0.0.1 productsearch.ubuntu.com" | sudo tee -a /etc/hosts >/dev/null
fi

echo "---------------------------------------"
echo "Removing MOTD spam"
echo "---------------------------------------"

# Remove MOTD spam from online sources
rm -f /etc/update-motd.d/10-help-text
rm -f /etc/update-motd.d/50-motd-news
rm -f /etc/update-motd.d/80-esm
rm -f /etc/update-motd.d/80-livepatch

echo "All done. Enjoy your privacy."
