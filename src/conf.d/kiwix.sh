#!/bin/bash

if [ ! -d $FAIR_DRIVE_MOUNTPOINT/data/kiwix ]
then
        echo 'Echo "Kiwix not found"'
        return
fi

echo "---------------------------------------"
echo "Kiwix (digital library)"
echo "---------------------------------------"

# Move the kiwix-serve binary to PATH
cp -Ruf $FAIR_DRIVE_MOUNTPOINT/data/kiwix/kiwix-serve /usr/bin

# Create the start-up script
copy_skel etc/init.d/kiwix
copy_skel etc/nginx/sites-available/004-kiwix
copy_skel etc/nginx/sites-enabled/004-kiwix

# Variable substitution in start-up script
sedeasy "{{ FAIR_DRIVE_MOUNTPOINT }}" "$FAIR_DRIVE_MOUNTPOINT" /etc/init.d/kiwix

# Make it executable
chmod +x /etc/init.d/kiwix

# Enable startup script
update-rc.d -f kiwix remove
update-rc.d kiwix defaults

# Start it
/etc/init.d/kiwix start
systemctl reload nginx
