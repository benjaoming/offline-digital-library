// This file can be used to configure global preferences for Firefox

// This is our default intranet
pref("browser.startup.homepage", "http://intranet.fair/");
pref("startup.homepage_welcome_url", "http://intranet.fair/");

// Disable fetching "What's New" page
pref("browser.startup.homepage_override.mstone", "ignore");

// Disable automatic extension updates
pref("browser.safebrowsing.downloads.remote.enabled", false);

// Disable Telemetry and pingbacks
pref("browser.selfsupport.url", "");

// Disable Captive Portal
pref("network.captive-portal-service.enabled", false);

// For now, we do not care about resuming because Firefox
// too frequently blocks the start page from displaying
pref("browser.sessionstore.resume_from_crash", false);

// Disable showing mozilla.org privacy page
pref("datareporting.policy.firstRunURL", "");
pref("toolkit.crashreporter.infoURL", "");
pref("toolkit.datacollection.infoURL", "");
