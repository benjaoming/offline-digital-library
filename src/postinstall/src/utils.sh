#!/bin/bash

# Easily make a replacement and escape the input and output strings
function sedeasy {
  sed -i "s/$(echo $1 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')/$(echo $2 | sed -e 's/[\/&]/\\&/g')/g" $3
}

# Delete a line containing $1 in file $2
function sedeasy_delete {
  sed -i "/$(echo $1 | sed -e 's/\([[\/.*]\|\]\)/\\&/g')/d" $2
}

# Copy $1 to its destination
# NOT IMPLEMENTED YET (NEEDS TO RECURSE FILES THEN)
# Replace all occurrences of {{2}} with argument at 2nd position etc.
# ...meaning {{1}} is replaced by file name.
function copy_skel {
  path_trimmed=$(echo "$1" | sed 's:/*$::')
  echo "Copying ${SCRIPT_ROOT}/skel/$path_trimmed to /$path_trimmed"

  source="${SCRIPT_ROOT}/skel/$path_trimmed"

  if [ -d "$source" ]
  then
    echo "Source is a directory, copying recursively..."
    if ! [ "$2" == "" ]
    then
      echo "Cannot substitute vars in directories yet... Not implemented."
      exit 1
    fi
    source_parent=`dirname "$path_trimmed"`
    mkdir -p "$path_trimmed"
    cp -Rfd "$source" "/$source_parent"
  else
    cp -fd "$source" "/$path_trimmed"
    for ((i=1; i<=$#; i++))
    do
      sedeasy "{{$i}}" ${!i} "/$1"
    done
  fi

}
