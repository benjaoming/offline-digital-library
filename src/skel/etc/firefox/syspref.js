// This file can be used to configure global preferences for Firefox

// This is our default intranet
lockPref("browser.startup.homepage", "http://intranet.fair/");

// Disable fetching "What's New" page
lockPref("browser.startup.homepage_override.mstone", "ignore");

// Disable automatic extension updates
lockPref("browser.safebrowsing.downloads.remote.enabled", false);

// Disable Telemetry and pingbacks
lockPref("browser.selfsupport.url", "");

// Disable Captive Portal
lockPref("network.captive-portal-service.enabled", false);
